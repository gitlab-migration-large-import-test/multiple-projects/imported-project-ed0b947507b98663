# Metrics

Application exposes several metrics that can be used to monitor its health and performance using [Prometheus](https://prometheus.io/) scraping. Metrics are exposed on `/metrics` endpoint if [`metrics`](../config/environment.md#metrics) option is enabled. Metrics are disabled by default.

## Type of Metrics

Following metrics are exposed.

### Generic metrics

* [sidekiq](https://github.com/yabeda-rb/yabeda-sidekiq#global-cluster-wide-metrics) - global cluster-wide metrics for Sidekiq

### Custom app metrics

* `vulnerabilities` gauge - number of merge requests with vulnerabilities and number of vulnerability issues per project
